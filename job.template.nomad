job "metrics-energy-svk" {
  name = "SVK Kontrolrummet Metrics"
  type = "batch"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"
  namespace = "energy"

  periodic {
    cron = "*/15 * * * *"
    prohibit_overlap = true
    time_zone = "[[ .defaultTimezone ]]"
  }

  vault {
    policies = ["job-metrics-energy-svk"]
  }

  group "svk" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "svk-metrics"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "kontrollrummet" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "[[ .jobDockerImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]

          {{ with secret "jobs/metrics-energy-svk/tasks/kontrollrummet/influx" }}
          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_ORG={{ index .Data.data "organisation" }}
          INFLUXDB_TOKEN={{ index .Data.data "token" }}
          INFLUXDB_BUCKET={{ index .Data.data "bucket" }}
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
