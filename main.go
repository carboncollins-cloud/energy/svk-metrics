package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
)

type KontrollrummetData struct {
	Name      int `json:"name"`
	SortOrder int `json:"sortorder"`
	Data      []struct {
		Timestamp int `json:"x"`
		Value     int `json:"y"`
	} `json:"data"`
}

type DataPoint struct {
	Time  time.Time
	Value int
}

func main() {
	client := influxdb2.NewClient(os.Getenv("INFLUXDB_HOST"), os.Getenv("INFLUXDB_TOKEN"))
	writeAPI := client.WriteAPI(os.Getenv("INFLUXDB_ORG"), os.Getenv("INFLUXDB_BUCKET"))

	errorsCh := writeAPI.Errors()
	// Create go proc for reading and logging errors
	go func() {
		for err := range errorsCh {
			fmt.Printf("influx: write error: %s\n", err.Error())
		}
	}()

	err := getProductionHistory("SE", &writeAPI)

	if err != nil {
		fmt.Sprintln("Error when getting history")
	}

	writeAPI.Flush()
	client.Close()
	fmt.Printf("Finished gathering metrics\n")
}

func getProductionHistory(zone string, writeAPI *api.WriteAPI) error {
	names := map[int]string{1: "production", 2: "nuclear", 3: "hydro", 4: "thermal", 5: "wind", 6: "other", 7: "consumption"}

	today := time.Now().Format("2006-01-02")
	fmt.Printf("%s\n", today)
	client := &http.Client{}
	request, _ := http.NewRequest("GET", fmt.Sprintf("https://www.svk.se/ControlRoom/GetProductionHistory?productionDate=%s&countryCode=%s", today, zone), nil)

	response, err := client.Do(request)

	if err != nil {
		return err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("svk: Latest data returned the status code: %d for country: %s", response.StatusCode, zone)
	}

	var controlRoomData []KontrollrummetData

	err = json.NewDecoder(response.Body).Decode(&controlRoomData)

	if err != nil {
		return err
	}

	for _, data := range controlRoomData {
		key := names[data.Name]

		if key != "" {
			for _, value := range data.Data {
				dataPoint := influxdb2.NewPointWithMeasurement("production").
					AddTag("country", zone).
					AddTag("unit", "MW").
					AddTag("source", "svk.se").
					AddField(key, value.Value).
					SetTime(time.UnixMilli(int64(value.Timestamp)))

				(*writeAPI).WritePoint(dataPoint)
			}
		}
	}

	return nil
}
